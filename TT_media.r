
library(tidyverse)
library(kableExtra)
media_franchises <- readr::read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-07-02/media_franchises.csv")

glimpse(media_franchises)

## Diversity of Walt Disneys revenue stream #### 


# Creating an overview of the most important franchise owner
media_franchises_company <- media_franchises %>% 
  mutate(is_WaltDisney = 0,
         is_Sony = 0,
         is_ATT = 0,
         is_Nintendo = 0,
         is_Shueisha = 0)

media_franchises_company[grep("Walt Disney", media_franchises$owners),"is_WaltDisney"] <- 1
media_franchises_company[grep("Sony", media_franchises$owners),"is_Sony"] <- 1
media_franchises_company[grep("AT&T", media_franchises$owners),"is_ATT"] <- 1
media_franchises_company[grep("Nintendo", media_franchises$owners),"is_Nintendo"] <- 1
media_franchises_company[grep("Shueisha", media_franchises$owners),"is_Shueisha"] <- 1

media_franchises_company$cooperation <- rowSums(media_franchises_company[,8:12])

### Show table of cooperation movies
kable(filter(media_franchises_company[,c(1:7,13)], cooperation == 2)) %>% 
  kable_styling(bootstrap_options = "striped", font_size = 14, full_width = F) %>% 
  column_spec(2, width = "30em")# only Sony and Walt Disney have cooperation Projects

  


### Is Disney the biggest player in the market? ####

# Checking for the biggest player in the market
media_franchises_company <- media_franchises_company %>% 
  mutate(company = ifelse(is_WaltDisney == 1 & is_Sony == 1,"Disney & Sony",
                          ifelse(is_WaltDisney == 1, "Walt Disney",
                                 ifelse(is_Sony == 1, "Sony", 
                                        ifelse(is_ATT == 1, "ATT",
                                               ifelse(is_Shueisha == 1, "Shueisha",
                                                      ifelse(is_Nintendo == 1, "Nintendo","Other")))))))
          
n_other <- media_franchises_company %>% 
  filter(company == "Other") %>% 
  group_by(owners) %>% 
  summarise(revenue = sum(revenue)) %>% 
  arrange(desc(revenue)) %>% 
  nrow()

media_franchises_company %>% 
  group_by(company) %>% 
  summarise(revenue = sum(revenue),
            n = n()) %>% 
  ggplot(aes(fct_reorder(company, revenue), revenue))+
  geom_col()+
  coord_flip()+
  theme_light()+
  theme(plot.background =  element_rect(fill = "#e8e8e8"),
        panel.background = element_rect(fill = "#e8e8e8"),
        legend.background = element_rect(fill = "#e8e8e8"),
        panel.grid.major.x = element_line(color = "#5c5c5c"),
        panel.grid.minor.x = element_line(color = "#5c5c5c"),
        panel.border = element_blank(),
        axis.text = element_text(color = "black", size = 10),
        panel.grid.major.y = element_blank(),
        panel.grid.minor.y = element_blank())+
  labs(title = "Media Companies",
       subtitle = "Who are the biggest companies?",
       x = "",
       y = "Revenue, in Billions")


### Diversity of Disney's Revenues #### 

# Use only Disney productions
walt_disney <- media_franchises_company %>% 
  filter(company == "Walt Disney" | company == "Disney & Sony") %>% 
  mutate(revenue_category = as.factor(revenue_category))

# Rename the revenue categories
walt_disney$revenue_category <- fct_recode(walt_disney$revenue_category, 
                                            "Books" = "Book sales",
                                            "Box Office" = "Box Office",
                                            "Comics" = "Comic or Manga",
                                            "Home Video" = "Home Video/Entertainment",
                                            "Merch" = "Merchandise, Licensing & Retail",
                                            "Music" = "Music",
                                            "TV" = "TV",
                                            "Video Games" = "Video Games/Games")

# reorder the categories, so similar one are shown together and.. 
walt_disney$revenue_category <- fct_relevel(walt_disney$revenue_category,
                                            "Box Office","Home Video", "TV",
                                            "Merch", "Music", "Video Games",
                                            "Books", "Comics")

# apply new modified colors
cp <- c("#e66350", "#ba5041", "#783329", "#addb46",
        "#87ab37", "#5b7325", "#fcd649", "#cfaf3c")

# getting the summarised revenue per franchise, to order by revenue in plot
wd_franchise_revenue <- walt_disney %>% 
  group_by(franchise) %>% 
  summarise(revenue_franchise = sum(revenue))

# add complete revenue to origin table
walt_disney_plot <- walt_disney %>% 
  left_join(wd_franchise_revenue, by = "franchise")

# Plotting it all together
ggplot(walt_disney_plot, aes(fct_reorder(franchise, revenue_franchise), revenue, fill = revenue_category))+
  geom_col()+
  coord_flip()+
  scale_fill_manual(values = cp)+
  theme_light()+
  theme(plot.background =  element_rect(fill = "#e8e8e8"),
        panel.background = element_rect(fill = "#e8e8e8"),
        legend.background = element_rect(fill = "#e8e8e8"),
        panel.grid.major.x = element_line(color = "#5c5c5c"),
        panel.grid.minor.x = element_line(color = "#5c5c5c"),
        panel.border = element_blank(),
        axis.text = element_text(color = "black", size = 10),
        panel.grid.major.y = element_blank(),
        panel.grid.minor.y = element_blank())+
  labs(title = "Walt Disney Franchises",
       subtitle = "How diverse is Walt Disneys revenue stream across franchises",
       x = "",
       y = "Revenue, in Billions")