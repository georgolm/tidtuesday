---
title: "Ufo Sightings"
author: "Georg Olm"
date: "7/3/2019"
output: html_document
editor_options: 
  chunk_output_type: console
---


```{r setup, include=FALSE}
library(tidyverse)
library(lubridate)

`%notin%` <- Negate(`%in%`)

ufo_sightings <- readr::read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-06-25/ufo_sightings.csv") %>% 
  rownames_to_column() 

ufo_date <- date(mdy_hm(ufo_sightings$date_time))
ufo_date_df <- data.frame(date_sight = ufo_date,
                          id = ufo_sightings$rowname) %>% 
  mutate(year_s = year(date_sight),
         month_s = month(date_sight),
         day_s = day(date_sight)) %>% 
  filter(year_s > 2000) 

ufo_outlier <- ufo_date_df %>% 
  group_by(date_sight) %>% 
  summarize(n = n()) %>% 
  filter(n > 50) 

ufo_country_outlier <- ufo_date_df %>% 
  filter(date_sight %notin% ufo_outlier$date_sight) %>% 
  left_join(ufo_sightings[,c(1,11,12)], by = c("id" = "rowname")) %>% 
  na.omit()

t <- ufo_country_outlier[ufo_country_outlier$country !="us" &
                          ufo_country_outlier$country !="ca" ,]
  

ggplot(ufo_country_outlier, aes(longitude, latitude))+
  geom_point(alpha = 0.05) +
  stat_density_2d()
  

```


# Racing Bar Chart
## What countries do aliens like to visit?
```{r}
rm(list = ls())
library(revgeo)
library(tidyverse)
library(lubridate)
library(jsonlite)
library(geonames)
ufo_sightings <- readr::read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-06-25/ufo_sightings.csv") %>% 
  rownames_to_column() 

ufo_date <- date(mdy_hm(ufo_sightings$date_time))
ufo_date_df <- data.frame(date_sight = ufo_date,
                          id = ufo_sightings$rowname,
                          shape = ufo_sightings$ufo_shape) %>% 
  mutate(year_s = year(date_sight),
         month_s = month(date_sight),
         day_s = day(date_sight))

ufo_outlier <- ufo_date_df %>% 
  group_by(year_s, month_s, shape) %>% 
  summarize(sightings = n()) %>% 
  na.omit()



```

